from flask import *
import sqlite3, hashlib, os
from werkzeug.utils import secure_filename
from datetime import datetime

app = Flask(__name__)
app.secret_key = 'random string'
UPLOAD_FOLDER = 'static/uploads'
ALLOWED_EXTENSIONS = set(['jpeg', 'jpg', 'png', 'gif'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
# 用户身份
guserIfo = {
    'userId': '',
    'userName': "",
    'phone': '',
    'address': "",
    'type': '',
    'loggedIn': False
}
noOfItems = 0


def parse(data):
    ans = []
    i = 0
    while i < len(data):
        curr = []
        for j in range(7):
            if i >= len(data):
                break
            curr.append(data[i])
            i += 1
        ans.append(curr)
    return ans


# def getLoginDetails():
#     with sqlite3.connect('database.db') as conn:
#         cur = conn.cursor()
#         if 'userName' not in session:
#             loggedIn = False
#             userName = ""
#             noOfItems = 0
#         else:
#             loggedIn = True
#             userName = session['userName']
#             noOfItems = 0
#             cur.execute("SELECT userId FROM users WHERE userName = ?", (session['userName']))
#             userId = cur.fetchone()
#             # if userId:
#             #     cur.execute("SELECT count(productId) FROM kart WHERE userId = ?", (userId))
#             #     noOfItems = cur.fetchone()[0]
#     conn.close()
#     return (loggedIn, userName, noOfItems)


@app.route("/")
def root():
    # loggedIn, userName, noOfItems = getLoginDetails()
    with sqlite3.connect('database.db') as conn:
        cur = conn.cursor()
        cur.execute('SELECT productId, name, price, description, image,categoryId FROM products')
        itemData = cur.fetchall()
        cur.execute('SELECT categoryId, name FROM categories')
        categoryData = cur.fetchall()
        items=[]
        for item in itemData:
            cur.execute('SELECT name FROM categories WHERE  categoryId=?',(item[5],))
            categoryName = cur.fetchone()
            items.append(item+categoryName)
    itemData = parse(items)
    return render_template('home.html', itemData=itemData, userIfo=guserIfo, categoryData=categoryData)


@app.route("/add")
def admin():
    with sqlite3.connect('database.db') as conn:
        cur = conn.cursor()
        cur.execute("SELECT categoryId, name FROM categories")
        categories = cur.fetchall()
    conn.close()
    return render_template('add.html', categories=categories)


@app.route("/addItem", methods=["GET", "POST"])
def addItem():
    if request.method == "POST":
        name = request.form['name']
        price = float(request.form['price'])
        description = request.form['description']
        stock = int(request.form['stock'])
        categoryId = int(request.form['category'])

        # Uploading image procedure
        image = request.files['image']
        if image and allowed_file(image.filename):
            filename = secure_filename(image.filename)
            image.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        imagename = filename
        with sqlite3.connect('database.db') as conn:
            try:
                cur = conn.cursor()
                cur.execute(
                    '''INSERT INTO products (name, price, description, image, stock, categoryId) VALUES (?, ?, ?, ?, ?, ?)''',
                    (name, price, description, imagename, stock, categoryId))
                conn.commit()
                msg = "added successfully"
            except:
                msg = "error occured"
                conn.rollback()
        conn.close()
        return redirect(url_for('root'))


@app.route("/remove")
def remove():
    with sqlite3.connect('database.db') as conn:
        cur = conn.cursor()
        cur.execute('SELECT productId, name, price, description, image, stock FROM products')
        data = cur.fetchall()
    conn.close()
    return render_template('remove.html', data=data)


@app.route("/removeItem")
def removeItem():
    productId = request.args.get('productId')
    with sqlite3.connect('database.db') as conn:
        try:
            cur = conn.cursor()
            cur.execute('DELETE FROM products WHERE productID = ?', (productId,))
            conn.commit()
            msg = "Deleted successsfully"
        except:
            conn.rollback()
            msg = "Error occured"
    conn.close()
    print(msg)
    return redirect(url_for('root'))


@app.route("/displayCategory")
def displayCategory():
    categoryId = request.args.get("categoryId")
    with sqlite3.connect('database.db') as conn:
        cur = conn.cursor()
        cur.execute(
            "SELECT products.productId, products.name, products.price, products.image, categories.name FROM products, categories WHERE products.categoryId = categories.categoryId AND categories.categoryId = ?",
            (categoryId,))
        data = cur.fetchall()
        # 查询数据，并按某列排序，返回前几条
        cur.execute(
            "SELECT products.productId, products.name, products.price, products.image, categories.name FROM products, categories WHERE products.categoryId = categories.categoryId AND categories.categoryId = ? ORDER BY products.changeTime DESC LIMIT 3",
            (categoryId,))
        recentD=[cur.fetchall()]
    conn.close()
    categoryName = request.args.get('name')
    data = parse(data)
    return render_template('displayCategory.html', data=data, userIfo=guserIfo, categoryName=categoryName,recentD=recentD)


@app.route("/account/profile")
def profileHome():
    if 'userName' not in session:
        return redirect(url_for('root'))
    # loggedIn, userName, noOfItems = getLoginDetails()
    return render_template("profileHome.html", userIfo=guserIfo, noOfItems=noOfItems)


@app.route("/account/profile/edit")
def editProfile():
    if 'userName' not in session:
        return redirect(url_for('root'))
    # loggedIn, userName, noOfItems = getLoginDetails()
    with sqlite3.connect('database.db') as conn:
        cur = conn.cursor()
        cur.execute(
            "SELECT userId, userName, userName, lastName, address1, address2, zipcode, city, state, country, phone FROM users WHERE userName = ?",
            (session['userName'],))
        profileData = cur.fetchone()
    conn.close()
    return render_template("editProfile.html", profileData=profileData, loggedIn=loggedIn, userName=userName,
                           noOfItems=noOfItems)


@app.route("/account/profile/changePassword", methods=["GET", "POST"])
def changePassword():
    if 'userName' not in session:
        return redirect(url_for('loginForm'))
    if request.method == "POST":
        oldPassword = request.form['oldpassword']
        oldPassword = hashlib.md5(oldPassword.encode()).hexdigest()
        newPassword = request.form['newpassword']
        newPassword = hashlib.md5(newPassword.encode()).hexdigest()
        with sqlite3.connect('database.db') as conn:
            cur = conn.cursor()
            cur.execute("SELECT userId, password FROM users WHERE userName = ?", (session['userName'],))
            userId, password = cur.fetchone()
            if (password == oldPassword):
                try:
                    cur.execute("UPDATE users SET password = ? WHERE userId = ?", (newPassword, userId))
                    conn.commit()
                    msg = "Changed successfully"
                except:
                    conn.rollback()
                    msg = "Failed"
                return render_template("changePassword.html", msg=msg)
            else:
                msg = "Wrong password"
        conn.close()
        return render_template("changePassword.html", msg=msg)
    else:
        return render_template("changePassword.html")


@app.route("/updateProfile", methods=["GET", "POST"])
def updateProfile():
    if request.method == 'POST':
        userName = request.form['userName']
        userName = request.form['userName']
        lastName = request.form['lastName']
        address1 = request.form['address1']
        address2 = request.form['address2']
        zipcode = request.form['zipcode']
        city = request.form['city']
        state = request.form['state']
        country = request.form['country']
        phone = request.form['phone']
        with sqlite3.connect('database.db') as con:
            try:
                cur = con.cursor()
                cur.execute(
                    'UPDATE users SET userName = ?, lastName = ?, address1 = ?, address2 = ?, zipcode = ?, city = ?, state = ?, country = ?, phone = ? WHERE userName = ?',
                    (userName, lastName, address1, address2, zipcode, city, state, country, phone, userName))

                con.commit()
                msg = "Saved Successfully"
            except:
                con.rollback()
                msg = "Error occured"
        con.close()
        return redirect(url_for('editProfile'))


@app.route("/loginForm")
def loginForm():
    # if 'userName' in session:
    #     return redirect(url_for('root'))
    # else:
    return render_template('login.html', error=False, userIfo=guserIfo)

# 用户身份
# guserIfo = {
#     'userId': '',
#     'userName': "",
#     'phone': '',
#     'address': "",
#     'type': '',
#     'loggedIn': False
# }
# todo
@app.route("/login", methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        userType = request.form['user-type']
        userName = request.form['userName']
        password = request.form['password']
        if is_valid(userType, userName, password):
            with sqlite3.connect('database.db') as conn:
                cur = conn.cursor()
                session['userName'] = userName
                # loggedIn = True
                if userType=="individual":
                    cur.execute("SELECT * FROM users WHERE userName = ?", (userName,))
                    guserIfo['type']=0
                else:
                    cur.execute("SELECT * FROM vendor WHERE userName = ?", (userName,))
                    guserIfo['type']=1
                    
        
                user = cur.fetchone()
                print(user)
                guserIfo['userId']=user[0]
                guserIfo['userName']=user[2]
                guserIfo['loggedIn']=True
                guserIfo['phone']=user[4]
                guserIfo['address']=user[3]
                return redirect(url_for('root'))
        else:
            error = 'Invalid userName / Password'
            return render_template('login.html', error=error, userIfo=guserIfo)


@app.route("/productDescription")
def productDescription():
    productId = request.args.get('productId')
    with sqlite3.connect('database.db') as conn:
        cur = conn.cursor()
        cur.execute(
            'SELECT productId, name, price, description, image, Number,discount,discountEnd,vendorName,like,dislike,categoryId FROM products WHERE productId = ?',
            (productId,))
        productData = cur.fetchone()
        cur.execute('SELECT name FROM categories WHERE  categoryId=?',(productData[11],))
        categoryName = cur.fetchone()
        # Retrieve existing comments for this product from database
        cur.execute('''SELECT commentId, userId, username, productId, content, timestamp 
                 FROM comment WHERE productId=?''', (productId,))
        raw_comments = cur.fetchall()
        comments = []
        for raw_comment in raw_comments:
            comment = list(raw_comment)
            timestamp_str = comment.pop()  # remove timestamp from comment tuple
            timestamp = datetime.strptime(timestamp_str, '%Y-%m-%d %H:%M:%S')
            comment.append(timestamp.strftime('%b %d, %Y at %I:%M %p'))
            comments.append(comment)

    conn.close()
    return render_template("productDescription.html", product=productData, userIfo=guserIfo, comments=comments,categoryName=categoryName)


# conn.execute('''CREATE TABLE products
# 		(productId INTEGER PRIMARY KEY,
# 		name TEXT,
# 		price REAL,
# 		description TEXT,
# 		image TEXT,
# 		vendorName TEXT,
# 		like INTEGER,
# 		dislike INTEGER,
# 		Number INTEGER,
# 		discount REAL,
# 		discountEnd TEXT,
# 		changeTime TEXT,
# 		categoryId INTEGER,
# 		vendorId INTEGER,
# 		FOREIGN KEY(categoryId) REFERENCES categories(categoryId),
# 		FOREIGN KEY(vendorId) REFERENCES vendor(vendorId)
# 		)''')

@app.route('/submit_review', methods=['POST'])
def add_comment():
    if 'userName' not in session:
        return redirect(url_for('root'))
    productId = request.args.get('productId')
    # Retrieve user ID from session, if available.
    # user_id = session.get('userId')

    # Retrieve username associated with user_id from database, if available.
    conn = sqlite3.connect('database.db')
    c = conn.cursor()
    # c.execute('''SELECT username FROM users WHERE userId=?''', (user_id,))
    # username = c.fetchone()[0] if c.fetchone() is not None else None
    user_id = guserIfo['userId']
    username = guserIfo['userName']
    # Insert new comment into database using the provided parameters.
    comment_content = request.form['comment']
    c.execute('''INSERT INTO comment (userId, username, productId, content) 
                 VALUES (?, ?, ?, ?)''', (user_id, username, productId, comment_content))
    conn.commit()

    # Redirect back to the product display page.
    return redirect(url_for('productDescription', productId=productId))


@app.route("/addToCart")
def addToCart():
    if 'userName' not in session:
        return redirect(url_for('loginForm'))
    else:
        productId = int(request.args.get('productId'))
        with sqlite3.connect('database.db') as conn:
            cur = conn.cursor()
            cur.execute("SELECT userId FROM users WHERE userName = ?", (session['userName'],))
            userId = cur.fetchone()[0]
            try:
                cur.execute("INSERT INTO kart (userId, productId) VALUES (?, ?)", (userId, productId))
                conn.commit()
                msg = "Added successfully"
            except:
                conn.rollback()
                msg = "Error occured"
        conn.close()
        return redirect(url_for('root'))


@app.route("/cart")
def cart():
    if 'userName' not in session:
        return redirect(url_for('loginForm'))
    # loggedIn, userName, noOfItems = getLoginDetails()
    userName = session['userName']
    with sqlite3.connect('database.db') as conn:
        cur = conn.cursor()
        cur.execute("SELECT userId FROM users WHERE userName = ?", (userName,))
        userId = cur.fetchone()[0]
        cur.execute(
            "SELECT products.productId, products.name, products.price, products.image FROM products, kart WHERE products.productId = kart.productId AND kart.userId = ?",
            (userId,))
        products = cur.fetchall()
    totalPrice = 0
    for row in products:
        totalPrice += row[2]
    return render_template("cart.html", products=products, totalPrice=totalPrice, userIfo=guserIfo)


@app.route("/removeFromCart")
def removeFromCart():
    if 'userName' not in session:
        return redirect(url_for('loginForm'))
    userName = session['userName']
    productId = int(request.args.get('productId'))
    with sqlite3.connect('database.db') as conn:
        cur = conn.cursor()
        cur.execute("SELECT userId FROM users WHERE userName = ?", (userName,))
        userId = cur.fetchone()[0]
        try:
            cur.execute("DELETE FROM kart WHERE userId = ? AND productId = ?", (userId, productId))
            conn.commit()
            msg = "removed successfully"
        except:
            conn.rollback()
            msg = "error occured"
    conn.close()
    return redirect(url_for('cart'))


@app.route("/logout")
def logout():
    session.pop('userName', None)
    guserIfo['loggedIn'] = False
    return redirect(url_for('root'))


def is_valid(loginType, userName, password):
    con = sqlite3.connect('database.db')
    cur = con.cursor()
    if loginType == "individual":

        guserIfo['type'] = 0
        cur.execute('SELECT userName, password,address,phone,userId FROM users')
    else:
        guserIfo['type'] = 1
        cur.execute('SELECT userName, password,address,phone,vendorId FROM vendor')

    data = cur.fetchall()
    for row in data:
        if row[0] == userName and row[1] == hashlib.md5(password.encode()).hexdigest():
            guserIfo['userName'] = row[0]
            guserIfo['address'] = row[2]
            guserIfo['phone'] = row[3]
            guserIfo['userId'] = row[4]
            guserIfo['loggedIn'] = True
            return True
    return False


@app.route("/register", methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        # Parse form data
        # conn.execute('''CREATE TABLE users
        # 		(userId INTEGER PRIMARY KEY,
        # 		password TEXT,
        # 		userName TEXT,
        # 		address TEXT,
        # 		phone TEXT
        # 		)''')

        password = request.form['password']
        userName = request.form['userName']
        address = request.form['address']
        phone = request.form['phone']
        userType = request.form['user-type']
        with sqlite3.connect('database.db') as con:
            try:
                cur = con.cursor()
                if userType == 'individual':
                    guserIfo['type'] = 0
                    cur.execute('INSERT INTO users (password, userName, address, phone) VALUES (?, ?, ?, ?)',
                                (hashlib.md5(password.encode()).hexdigest(), userName, address, phone))
                else:
                    guserIfo['type'] = 1
                    cur.execute('INSERT INTO vendor (password, userName, address, phone) VALUES (?, ?, ?, ?)',
                                (hashlib.md5(password.encode()).hexdigest(), userName, address, phone))
                con.commit()

                msg = "Registered Successfully"
            except:
                con.rollback()
                msg = "Error occured"
        con.close()
        return render_template("login.html", error=msg, userIfo=guserIfo)


@app.route("/registerationForm")
def registrationForm():
    return render_template("register.html", userIfo=guserIfo)


@app.route("/upload", methods=['POST', 'GET'])
def upload_product():
    if 'userName' not in session:
        return redirect(url_for('root'))
    if request.method == 'POST':
        # 获取表单数据
        name = request.form['name']
        num = request.form['num']
        price = float(request.form['price'])
        discount_price = float(request.form['discount_price'] or 0)
        deadline_str = request.form['deadline']
        deadline = datetime.strptime(deadline_str, '%Y-%m-%d').date() if deadline_str else None
        description = request.form['description']

        # 处理上传图片
        image_file = request.files['image_file']
        if image_file:
            filename = image_file.filename
            image_path = os.path.join(app.config["UPLOAD_FOLDER"], filename)
            image_file.save(image_path)
            image_path = filename
        else:
            image_path = None

        # 获取商品类别信息，并存储到数据库中
        category_name = request.form['category']
        conn = sqlite3.connect('database.db')
        c = conn.cursor()
        c.execute("SELECT categoryId FROM categories WHERE name=?", (category_name,))
        category_id = c.fetchone()[0]

        c.execute(
            "INSERT INTO products (name, price, discount, discountEnd, description, image, categoryId,Number,vendorName,vendorId,like,dislike,changeTime) VALUES ( ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?)",
            (name, price, discount_price, deadline, description, image_path, category_id, num, guserIfo['userName'],
             guserIfo['userId'], 0, 0, '0'))
        conn.commit()
        # (productId INTEGER PRIMARY KEY,
        # name TEXT,
        # price REAL,
        # description TEXT,
        # image TEXT,
        # vendorName TEXT,
        # like INTEGER,
        # dislike INTEGER,
        # Number INTEGER,
        # discount REAL,
        # discountEnd TEXT,
        # changeTime TEXT,
        # categoryId INTEGER,
        # vendorId INTEGER,
        # FOREIGN KEY(categoryId) REFERENCES categories(categoryId),
        # FOREIGN KEY(vendorId) REFERENCES vendor(vendorId)

        # 跳转到商品列表页面
        flash('上传成功!')
        c.execute("SELECT name FROM categories ORDER BY categoryId")
        categories = [row[0] for row in c.fetchall()]
        # return redirect(url_for('.'))
        conn.close()
        return render_template('uploadGoods.html', categories=categories, userIfo=guserIfo)

        # return True
    else:
        # 显示上传商品的表单
        conn = sqlite3.connect('database.db')
        c = conn.cursor()
        c.execute("SELECT name FROM categories ORDER BY categoryId")
        categories = [row[0] for row in c.fetchall()]
        conn.close()
        return render_template('uploadGoods.html', categories=categories, userIfo=guserIfo)


@app.route('/product/<int:product_id>', methods=['GET', 'POST'])
def product_detail(product_id):
    conn = sqlite3.connect('database.db')
    c = conn.cursor()

    if request.method == 'POST':
        now = datetime.now()
        current_time = now.strftime("%Y-%m-%d %H:%M:%S")
        # 喜欢按钮
        if request.form['action'] == 'like':
            conn.execute(f"UPDATE products SET like=like+1 WHERE productId={product_id}")
            c.execute("UPDATE products SET changeTime=? WHERE productId = ?", (current_time,product_id))
        # 不喜欢按钮
        elif request.form['action'] == 'dislike':
            conn.execute(f"UPDATE products SET dislike=dislike+1 WHERE productId={product_id}")
            c.execute("UPDATE products SET changeTime=? WHERE productId = ?", (current_time,product_id))

        conn.commit()
        conn.close()
    return redirect(url_for('productDescription', productId=product_id))


# @app.route('/add_to_cart', methods=['POST'])
# def add_to_cart():
#     if request.method == 'POST':
#         product_id = request.form['product_id']  # 从请求中获取商品 ID
#         user_id = guserIfo['userId']  # 获取当前用户 ID

#         conn.execute('INSERT INTO kart VALUES (?, ?, 1)', (user_id, product_id))  # 将数据插入到 kart 表格中。
#         conn.commit()
#     return jsonify({'success': True}), 200  # 返回 JSON 响应，指示操作是否执行成功。

@app.route('/add_to_cart', methods=['POST'])
def add_to_cart():
    conn = sqlite3.connect('database.db')
    if request.method == 'POST':
        user_id = guserIfo['userId']  # 获取当前用户 ID
        product_id = request.form['product_id']  # 从请求中获取商品 ID
        now = datetime.now()
        current_time = now.strftime("%Y-%m-%d %H:%M:%S")
        c=conn.cursor()
        c.execute("UPDATE products SET changeTime=? WHERE productId = ?", (current_time,product_id))

        existing_row = conn.execute('SELECT * FROM kart WHERE userId=? AND productId=?',
                                    (user_id, product_id)).fetchone()
        if existing_row is not None:  # 如果找到符合条件的已存在行，则更新其数量
            current_quantity = existing_row[2]
            conn.execute('UPDATE kart SET productNum=? WHERE userId=? AND productId=?',
                         (current_quantity + 1, user_id, product_id))
        else:  # 如果没有找到符合条件的行，则插入新行
            conn.execute('INSERT INTO kart VALUES (?, ?, 1)', (user_id, product_id))

        conn.commit()

    return jsonify({'success': True}), 200  # 返回 JSON 响应，指示操作是否执行成功。


@app.route('/add_to_favorite', methods=['POST'])
def add_to_favorite():
    if 'userName' not in session:
        return redirect(url_for('root'))
    conn = sqlite3.connect('database.db')
    if request.method == 'POST':
        user_id = guserIfo['userId']  # 获取当前用户 ID
        product_id = request.form['product_id']  # 从请求中获取商品 ID
        now = datetime.now()
        current_time = now.strftime("%Y-%m-%d %H:%M:%S")
        c=conn.cursor()
        c.execute("UPDATE products SET changeTime=? WHERE productId = ?", (current_time,product_id))

        existing_row = conn.execute('SELECT * FROM favorite WHERE userId=? AND productId=?',
                                    (user_id, product_id)).fetchone()
        if existing_row is None:  # 如果找不到符合条件的已存在行，则插入新行
            conn.execute('INSERT INTO favorite VALUES (?, ?)', (user_id, product_id))
            conn.commit()

    return jsonify({'success': True}), 200  # 返回 JSON 响应，指示操作是否执行成功。


@app.route('/favorite_list', methods=['GET'])
def favorite_list():
    conn = sqlite3.connect('database.db')
    if guserIfo['loggedIn'] == False:
        return redirect(url_for('login'))

    user_id = guserIfo['userId']
    rows = conn.execute('SELECT * FROM products WHERE productId IN (SELECT productId FROM favorite WHERE userId=?)',
                        (user_id,)).fetchall()
    cursor=conn.cursor()
    items=[]
    for item in rows:
        cursor.execute('SELECT name FROM categories WHERE  categoryId=?',(item[12],))
        categoryName = cursor.fetchone()
        items.append(item+categoryName)

    return render_template('favorite.html',data=items, userIfo=guserIfo)


# @app.route('/product/<int:product_id>')
# def product_detail(product_id):
#     # 根据所提供的产品 ID 从数据库中检索单个商品记录
#     # ...
#     return redirect(url_for('productDescription', productId=product_id))


# todo 更新购物车，
@app.route('/updateCart')
def update_cart():
    conn = sqlite3.connect('database.db')

    productId = request.args.get('productId')
    productNum = request.args.get('productNum')

    # 更新数据库yyf
    conn.execute("UPDATE kart SET productNum = ? WHERE productId=? AND userId = ?",
                 (productNum, productId, guserIfo['userId']))

    conn.commit()

    return 'success'


@app.route('/checkout')
def orders():
    # 获取当前登录用户的ID
    conn = sqlite3.connect('database.db')
    userId = guserIfo['userId']
    c = conn.cursor()
    c.execute(
        "SELECT products.productId, products.name, products.price, products.image,kart.productNum,products.categoryId FROM products, kart WHERE products.productId = kart.productId AND kart.userId = ?",
        (userId,))
    products = c.fetchall()
    pList = []
    totalPrice = 0
    for item in products:
        totalPrice += item[2] * item[4]
        c.execute('SELECT name FROM categories WHERE  categoryId=?',(item[5],))
        categoryName = c.fetchone()
        pList.append(item+categoryName)
    conn.close()
    return render_template("order.html", products=pList, totalPrice=totalPrice, userIfo=guserIfo)

# //todo
@app.route('/confirmOrder')
def confirmOrder():
    # 获取当前登录用户的ID
    conn = sqlite3.connect('database.db')
    userId = guserIfo['userId']
    c = conn.cursor()
    c.execute(
        "SELECT products.productId, products.name, products.price, products.image,kart.productNum,products.categoryId FROM products, kart WHERE products.productId = kart.productId AND kart.userId = ?",
        (userId,))
    products = c.fetchall()
    pList = []
    totalPrice = 0
    for item in products:
        totalPrice += item[2] * item[4]
        c.execute('SELECT name FROM categories WHERE  categoryId=?',(item[5],))
        categoryName = c.fetchone()
        print(categoryName)
        pList.append(item+categoryName)
    conn.close()
    return render_template("order.html", products=pList, totalPrice=totalPrice, userIfo=guserIfo)


@app.route('/confirmPay', methods=['POST'])
def confirmPay():
    # 获取表单提交的信息（地址、电话号码等）
    # address = request.form['address']
    # phone = request.form['phone']

    # 连接到数据库
    conn = sqlite3.connect('database.db')

    # 获取当前登录用户的id
    userId = guserIfo['userId']
    # 根据你的业务逻辑获取userId

    # 从购物车表(kart)中查询用户要购买的商品信息
    carr = conn.cursor()
    select_sql = "SELECT * FROM kart WHERE userId=?"
    carr.execute(select_sql, [userId])
    cart_list = carr.fetchall()

    query_sql = 'SELECT vendorId FROM products WHERE productId=?'

    now = datetime.now()  # 获取当前时间
    current_time = now.strftime("%Y-%m-%d %H:%M:%S")  # 将当前时间转换为指定格式的字符串

    # 生成订单
    oarr = conn.cursor()
    for item in cart_list:
        # 插入订单表中
        carr.execute(query_sql, [item[1]])
        row = carr.fetchone()
        vendorId = row[0]

        insert_sql = "INSERT INTO orders(userId, productId, productNum, vendorId, phone, address,payTime,finish) VALUES(?,?,?, ?, ?, ?, ?, ?)"
        carr.execute(insert_sql, [userId, item[1], item[2], vendorId, gIfo['phone'], gIfo['address'], current_time, 0])

        # 将购物车表(kart)中对应的商品项删除
        delete_sql = "DELETE FROM kart WHERE userId=? and productId=?"
        carr.execute(delete_sql, [userId, item[1]])
    # (orderId INTEGER PRIMARY KEY,
    # 		productId INTEGER,
    # 		productNum INTEGER,
    # 		userId INTEGER,
    # 		vendorId INTEGER,
    # 		phone TEXT,
    # 		address TEXT,
    # 		payTime TEXT,
    # 		finish INTEGER,
    conn.commit()
    conn.close()

    return redirect(url_for('orderSuccess'))

# todo
gIfo={
'address':'',
'phone':''
}
@app.route('/confirmPay1', methods=['POST'])
def confirmPay1():
    # 获取表单提交的信息（地址、电话号码等）
    gIfo['address'] = request.form['address']
    gIfo['phone'] = request.form['phone']
    return render_template('zhiFu.html')

# 订单提交成功页面
@app.route('/orderSuccess')
def orderSuccess():
    return redirect(url_for('root'))


@app.route('/horders')
def horders():
    userType = guserIfo['type']
    userId = guserIfo['userId']
    userName=guserIfo['userName']
    vendorName=guserIfo['userName']
    conn = sqlite3.connect('database.db')
    if userType == 0:
        query_sql = '''
			SELECT orderId,products.name, productNum,
				products.vendorId, userId, phone, address, payTime, finish,products.price,products.image
			FROM orders 
			INNER JOIN products ON orders.productId = products.productId 
			WHERE userId = ?
		'''
        cur = conn.cursor()
        cur.execute(query_sql, [userId])
        orders = cur.fetchall()
        data=[]
        for order in orders:
        # 查询数据，并按某列逆序排序，返回前几条
            cur.execute("SELECT userName FROM users WHERE userId = ?",(order[4],))
            userName= cur.fetchone()
            cur.execute("SELECT userName FROM vendor WHERE vendorId = ?",(order[3],))
            vendorName= cur.fetchone()
            order=order+userName+vendorName
            data.append(order)
        conn.close()

    else:
        query_sql = '''
			SELECT orderId,products.name, productNum,
				products.vendorId, userId, phone, address, payTime, finish,products.price,products.image
			FROM orders 
			INNER JOIN products ON orders.productId = products.productId 
			WHERE userId = ?
		'''
        cur = conn.cursor()

        cur.execute(query_sql, [userId])
        orders = cur.fetchall()
        data=[]
        for order in orders:
        # 查询数据，并按某列逆序排序，返回前几条
            cur.execute("SELECT userName FROM users WHERE userId = ?",(order[4],))
            userName= cur.fetchone()
            cur.execute("SELECT userName FROM vendor WHERE vendorId = ?",(order[3],))
            vendorName= cur.fetchone()
            order=order+userName+vendorName
            data.append(order)
        # c.execute("SELECT userName FROM users WHERE userId = ?",(userId))

        conn.close()
    return render_template('horder.html', orders=data, userId=userId,userIfo=guserIfo)

# 定义路由和视图函数
@app.route('/finish_order/<orderid>')
def finish_order(orderid):
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()

    cur.execute("UPDATE orders SET finish = ? WHERE orderId = ?", (1, orderid))
    conn.commit()
    conn.close()
    # 订单已经完成，重定向到首页或者其中某个已完成订单页面
    return redirect(url_for('horders'))

@app.route('/search', methods=['POST', 'GET'])
def search():
    if request.method == 'POST':
        searchQuery = request.form['searchQuery']
        conn = sqlite3.connect('database.db')
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM products WHERE name LIKE ?", ('%' + searchQuery + '%',))
        results = cursor.fetchall()
        items=[]
        for item in results:
            cursor.execute('SELECT name FROM categories WHERE  categoryId=?',(item[12],))
            categoryName = cursor.fetchone()
            items.append(item+categoryName)
    # conn = sqlite3.connect('database.db')
    # cur = conn.cursor()

    # cur.execute("UPDATE orders SET finish = ? WHERE orderId = ?", (1, orderid))
    # conn.commit()
    # conn.close()
    # 订单已经完成，重定向到首页或者其中某个已完成订单页面
        return render_template("search.html",userIfo=guserIfo,searchQuery=searchQuery,data=items)



if __name__ == '__main__':
    app.run(debug=True)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


if __name__ == '__main__':
    app.run(debug=True)
