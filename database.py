import sqlite3

#Open database
conn = sqlite3.connect('database.db')

#Create table
conn.execute('''CREATE TABLE users 
		(userId INTEGER PRIMARY KEY, 
		password TEXT,
		userName TEXT,
		address TEXT,
		phone TEXT
		)''')

conn.execute('''CREATE TABLE vendor 
		(vendorId INTEGER PRIMARY KEY, 
		password TEXT,
		userName TEXT,
		address TEXT,
		phone TEXT
		)''')

conn.execute('''CREATE TABLE products
		(productId INTEGER PRIMARY KEY,
		name TEXT,
		price REAL,
		description TEXT,
		image TEXT,
		vendorName TEXT,
		like INTEGER,
		dislike INTEGER,
		Number INTEGER,
		discount REAL,
		discountEnd TEXT,
		changeTime TEXT,
		categoryId INTEGER,
		vendorId INTEGER,
		FOREIGN KEY(categoryId) REFERENCES categories(categoryId),
		FOREIGN KEY(vendorId) REFERENCES vendor(vendorId)
		)''')

conn.execute('''CREATE TABLE comment
             (commentId INTEGER,
              userId INTEGER,
              username TEXT,
              productId INTEGER,
              content TEXT,
              timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY(commentId),
              FOREIGN KEY(userId) REFERENCES users(userId),
              FOREIGN KEY(productId) REFERENCES products(productId)
             )''')

conn.execute('''CREATE TABLE kart
		(userId INTEGER,
		productId INTEGER,
		productNum INTEGER,
		FOREIGN KEY(userId) REFERENCES users(userId),
		FOREIGN KEY(productId) REFERENCES products(productId)
		)''')

conn.execute('''CREATE TABLE categories
		(categoryId INTEGER PRIMARY KEY,
		name TEXT
		)''')

# 创建游标对象
cur = conn.cursor()
# 编写sql语句
sql = 'insert into categories(categoryId, name) values(?, ?) '
# 执行sql语句
cur.executemany(sql, [(1,'Electronic products'),(2,'clothing'),(3,'furniture'),(4,'toy'),(5,'food')])
# 提交事务
conn.commit()


conn.execute('''CREATE TABLE favorite
		(userId INTEGER,
		productId INTEGER,
		FOREIGN KEY(userId) REFERENCES users(userId),
		FOREIGN KEY(productId) REFERENCES products(productId)
		)''')


conn.execute('''CREATE TABLE history
		(userId INTEGER,
		productId INTEGER,
		finishT TEXT,
		FOREIGN KEY(userId) REFERENCES users(userId),
		FOREIGN KEY(productId) REFERENCES products(productId)
		)''')

conn.execute('''CREATE TABLE orders
		(orderId INTEGER PRIMARY KEY,
		productId INTEGER,
		productNum INTEGER,
		userId INTEGER,
		vendorId INTEGER,
		phone TEXT,
		address TEXT,
		payTime TEXT,
		finish INTEGER,
		FOREIGN KEY(userId) REFERENCES users(userId),
		FOREIGN KEY(productId) REFERENCES products(productId),
		FOREIGN KEY(vendorId) REFERENCES vendor(vendorId)
		)''')


conn.close()

